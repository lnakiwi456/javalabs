Hi Kira,

Great job!  I'm really glad that you got through the labs up to inheritance, but wish you had gotten to Interfaces (most important feature in Java), Collections, and Exceptions as well.  They make Spring much easier to understand. 

Account: 
 - Nice job on withdraw(double amount) does exactly what it should.  However it returns from the middle of if statements.  The best practice is to only have one return at the end of the method.  This makes the code more understandable to others needing to maintain it once you are off on your next project, especially if the method is more complex method than this one.
 - And You repeat the same logic in the withdraw() method instead of making withdraw(double amount) do all the work!  This violates the DRY principal, Do Not Repeat Yourself.  If the same code appears in multiple places, someone updating one place may not know to update all the places the logic appears.  This leads to really strange behavior and race conditions.  Instead just have withdraw() call withdraw(double amont) with the amount of $20.  Something like this:
 
 	public boolean withdraw()	{
		return withdraw(20.00);
	}

	public boolean withdraw(double amount)	{
		
		boolean flag = false;
		if ((balance-amount) > 0)		{
			balance -= amount;
			flag = true;
			System.out.println(name + " you have successfully withdrawn " + amount +".");
			System.out.println("There is now " + balance + " left in your account.");
		}
		else  {
			System.out.println(name + ", sorry not enough money. You only have " + balance);
		}
		return flag;
	}

Did you do more work than this and just not push it up to BitBucket?  If so I'd love to know how you were doing at the end of week, as your last push was the middle of the week?

 Also, I know the labs said to just keep adding to the same files.  But when the new labs break the old ones (the test classes show up in red due to compilation errors), please copy the files to a different different folders to continue.  This will keep you from having to say proceed on each run since you have compilation errors, and it will help me as well.  It's really hard for me to figure out which class goes with which lab when they are all jumbled together, especially when some labs can't run anymore due to compilation errors introduced in later labs.  tks... ;~D
 
 
 
