public class TestInheritance {
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[3];

        arrayOfAccounts[0] = new SavingsAccount("Ben", 2);
        arrayOfAccounts[1] = new SavingsAccount("Jill", 4);
        arrayOfAccounts[2] = new CurrentAccount("Steven", 6);

        for (int i=0; i < arrayOfAccounts.length; i++) {

            System.out.printf("Acount Name: %s\nAccount Balance: $%.2f\n", arrayOfAccounts[i].getName(), arrayOfAccounts[i].getBalance());

            arrayOfAccounts[i].addInterest();
            System.out.printf("Balance After Interest: $%.2f\n", arrayOfAccounts[i].getBalance());
            System.out.println("\n");
        }

    }
}