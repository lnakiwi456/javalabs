public class SavingsAccount extends Account {
    // Properties

    // Constructors
    public SavingsAccount(String name, double balance) {
        // call to superclass constructor
        super (name, balance);

    }

    // Methods
    @Override
    public void addInterest() {
        this.setBalance(this.getBalance() *  1.4);
    }
}