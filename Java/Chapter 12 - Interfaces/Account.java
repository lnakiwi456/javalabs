public abstract class Account implements Detailable {
    // Properties
    private double balance;
    private String name;
    private static double interestRate = 0.10;

    // Constructors
    Account() {
        this.name = "Kira";
        this.balance = 50;
    }

    Account(String inputName, double inputBalance) {
        this.name = inputName;
        this.balance = inputBalance;
    }

    // Methods
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public abstract void addInterest(); 
        // double newBalance = (this.balance * interestRate) + this.balance;

        // this.setBalance(newBalance);
    

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount) {
        // If amount < current balance, withdraw and return true. Else, return false
        if (amount < this.getBalance()) {
            double newBalance = this.getBalance() - amount;
            this.setBalance(newBalance);
            System.out.printf("%.2f withdrawn from account.\nNew balance: %.2f", amount, this.getBalance());

            return true;
        }
        else {
            System.out.println("Account does not contain enough money to withdraw");
            return false;
        }
    }

    public boolean withdraw() {
        // If withdraw(20) works, return true. Else return false. Note: if true, withdraw(20) should have already withdrawn money.
        if (this.withdraw(20) == true) {
            return true;
        }
        else {
            return false;
        }
    }

    public String getDetails() {
        return String.format("Name: %s\nBalance: %.2f", this.getName(), this.getBalance());
    }

}