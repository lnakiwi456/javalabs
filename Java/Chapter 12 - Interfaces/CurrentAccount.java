public class CurrentAccount extends Account {

    // Constructors
    public CurrentAccount(String name, double balance) {
        // call to superclass constructor
        super (name, balance);
        
    }

    // Methods
    @Override
    public void addInterest() {
        this.setBalance(this.getBalance() *  1.1);
    }
}