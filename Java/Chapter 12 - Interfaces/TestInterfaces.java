public class TestInterfaces {
    public static void main(String[] args) {

    Detailable[] arrayOfDetailables = new Detailable[3];

    arrayOfDetailables[0] = new HomeInsurance();
    arrayOfDetailables[1] = new SavingsAccount("Jill", 100);
    arrayOfDetailables[2] = new CurrentAccount("Steven", 2000);

    for (int i=0; i < arrayOfDetailables.length; i++) {

        System.out.println(arrayOfDetailables[i].getDetails());
        System.out.println("\n");
    }

    }
}