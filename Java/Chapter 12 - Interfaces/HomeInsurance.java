public class HomeInsurance implements Detailable {
    // Properties
    private double amountInsured;
    private double excess;
    private double premium;

    // Constructors

    HomeInsurance() {
        this.amountInsured = 100.00;
        this.excess = 30.00;
        this.premium = 70.00;
    }

    HomeInsurance(double amountInsured, double excess, double premium) {
        this.amountInsured = amountInsured;
        this.excess = excess;
        this.premium = premium; 
    }

    // Methods

    public double getAmountInsured() {
        return amountInsured;
    }

    public void setAmountInsured(double amountInsured) {
        this.amountInsured = amountInsured;
    }

    public double getExcess() {
        return excess;
    }

    public void setExcess(double excess) {
        this.excess = excess;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }

    @Override
    public String getDetails() {
        return String.format("Amount Insured: %.2f\nExcess: %.2f\nPremium: %.2f", this.getAmountInsured(), this.getExcess(), this.getPremium());
    }
    

}