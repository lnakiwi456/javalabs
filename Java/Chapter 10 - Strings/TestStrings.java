import java.text.SimpleDateFormat;
import java.util.Date;

public class TestStrings {
    public static void main(String[] args) {
        
        /* 
            --- Chapter 10, Part 1-2 ---
            Using a String with the value example.doc, change the String to example.bak
        */
        String string1 = "example.doc";

        // Find the index of '.' character in string 1
        int indexOfDot = string1.indexOf('.');

        // create new string by spliting string1 up to (and including) '.' and appending 'bak'
        String string2 = string1.substring(0, indexOfDot+1) + "bak";
        System.out.println(string2);
        System.out.println("\n");

        /* 
            --- Chapter 10, Part 1-3 --
            Input two Strings and test them see if they are equal. If they are not equal show which 
            of them lexicographically further forward (in the dictionary!)
        */

        String string3 = "what's";
        String string4 = "up";

        // Test if strings are equal
        if (string3.equals(string4)) {
            System.out.println(string3 + " and " + string4 + " are equal.");
        }
        else {
            System.out.println(string3 + " and " + string4 + " are NOT equal.");
            if (string3.compareTo(string4) >= 1) {
                System.out.printf("\"%s\" is further foward than \"%s\"", string3, string4);
            }
            else {
                System.out.printf("\"%s\" is further foward than \"%s\"", string4, string3);
            }
        }

        System.out.println("\n");

        /* 
            --- Chapter 10, Part 1-4 --
            Find the number of times "ow" occurs in "the quick brown fox swallowed down the lazy chicken"
        */

        String phrase = "the quick brown fox swallowed down the lazy chicken";
        int numOfOcurr = 0;

        for (int i = 0; i < phrase.length(); i++) {
            if (phrase.charAt(i) == 'o') {
                // Continue if not on last character in string
                if (i != phrase.length() - 1) {
                    if (phrase.charAt(i + 1) == 'w') {
                        numOfOcurr++;
                    }
                }
            }
        }

        System.out.printf("Occurances of \"ow\": %d", numOfOcurr);
        System.out.println("\n");

        /* 
            --- Chapter 10, Part 1-5 --
            Check to see whether a given string is a palindrome (eg "Live not on evil")
        */

        String string5 = "Live not on evil";
        String string5Transformed = string5.replaceAll(" ", "").toLowerCase();
    

        String reversedString = "";

        for (int i = string5Transformed.length() - 1; i > -1; i--) {
            reversedString += string5Transformed.charAt(i);
        }

        if (string5Transformed.equals(reversedString)) {
            System.out.printf("%s is a palindrome!", string5);
        }
        else {
            System.out.printf("%s is NOT a palindrome!", string5);
        }
        System.out.println("\n");


        /* 
            --- Chapter 10, Part 2 --
            Print today's date in various formats.
        */

        // Establish some patterns
        String pattern1 = "MM-dd-yyyy";
        String pattern2 = "MM/dd/yyyy";
        String pattern3 = "dd-MM-yyyy";

        // Create date objects using patterns
        SimpleDateFormat pattern1Date = new SimpleDateFormat(pattern1);
        SimpleDateFormat pattern2Date = new SimpleDateFormat(pattern2);
        SimpleDateFormat pattern3Date = new SimpleDateFormat(pattern3);

        String date1 = pattern1Date.format(new Date());
        String date2 = pattern2Date.format(new Date());
        String date3 = pattern3Date.format(new Date());
        System.out.println(date1);
        System.out.println(date2);
        System.out.println(date3);

    }
}